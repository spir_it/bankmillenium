﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BankMillenium.Service;
using BankMillenium.Extensions;
using BankMillenium.Models;
using BankMillenium.Controllers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using NSubstitute;
using Xunit;

namespace BankMillenium.Tests.Controllers
{
    public class PersonDetailsControllerTests
    {
        [Fact]
        public void GetAllPersons_WhenCalled_ReturnsAllItems()
        {
            // Arrange
            var controller = GetPersonsDetailController();

            // Act
            var result = controller.GetAllPersons() as List<Person>;

            // Assert
            Assert.True(result is {Count: 3});
        }

        private PersonDetailsController GetPersonsDetailController()
        {
            var personsService = Substitute.For<IPersonService>();

            personsService.GetAllPersons().Returns(info => new List<Person>()
            {
                Substitute.For<Person>(),
                Substitute.For<Person>(),
                Substitute.For<Person>(),
            });

            personsService.AddPerson(new Person()).Returns(info => info.Arg<Person>());

            personsService.DeletePerson(null).Returns(_ => true);

            personsService.UpdatePerson(new Person()).Returns(_ => true);

            personsService.GetPersonByEmail("").Returns(info =>
            {
                var person = Substitute.For<Person>();

                person.UserEmail.Returns(info.Arg<string>());

                return person;
            });

            var loggerFactory = Substitute.For<ILoggerFactory>();
            var logger = Substitute.For<ILogger>();
            loggerFactory.CreateLogger("").Returns(logger);

            return new PersonDetailsController(personsService, loggerFactory);
        }

        private IEnumerable<Person> GetAllPersons(PersonDetailsController controller) =>
            controller.GetAllPersons() as IEnumerable<Person>;

        [Fact]
        public async void AddPerson_WhenCalled_AddsPerson()
        {
            // Arrange 
            var controller = GetPersonsDetailController();

            // Act
            var result = await controller.AddPerson(Substitute.For<Person>());

            // Assert
            Assert.True((bool) result);
        }

        [Fact]
        public void DeletePerson_WhenCalled_RemovesPerson()
        {
            // Arrange
            var controller = GetPersonsDetailController();

            // Act
            var result = controller.DeletePerson("antoni.maciArewicz@gmail.com");

            // Assert
            Assert.True(result);
        }

        [Fact]
        public void UpdatePerson_WhenCalled_UpdatesPerson()
        {
            // Arrange
            var controller = GetPersonsDetailController();

            // Act
            var result = controller.UpdatePerson(Substitute.For<Person>());

            // Assert
            Assert.True(result);
        }

        [Fact]
        public void GetPersonByEmail_WhenCalled_ReturnsPerson()
        {
            // Arrange
            var email = "antoni.maciArewicz@gmail.com";
            var controller = GetPersonsDetailController();

            var loggerFactory = Substitute.For<ILoggerFactory>();
            var logger = Substitute.For<ILogger>();
            loggerFactory.CreateLogger("").Returns(logger);

            var l = loggerFactory.CreateLogger("");

            var p = Substitute.For<Person>();
            p.UserEmail.Returns(email);
            //p.UserEmail?.Returns(email);


            // Act
            var person = controller.GetPersonByEmail(email) as Person;

            // Assert
            Assert.Equal(person.UserEmail, email);
        }
    }
}
