﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Castle.Core.Internal;
using BankMillenium.Service;
using BankMillenium.Extensions;
using BankMillenium.Models;
using NSubstitute;

namespace BankMillenium.Tests
{
    public class PersonServiceFake : IPersonService
    {
        private readonly IList<Person> persons;

        public PersonServiceFake()
        {
            persons = new List<Person>()
            {
                new()
                {
                    Id = 1,
                    CreatedOn = DateTime.Parse("2021-11-04T10:52:53.0870646+02:00"),
                    UserEmail = "mchylek01@gmail.com",
                    IsDeleted = false,
                    UserName = "Marcin",
                    UserPassword = "unknown",
                },

                new()
                {
                    Id = 2,
                    CreatedOn = DateTime.Parse("2021-11-04T10:52:53.0870646+02:00"),
                    UserEmail = "antoni.maciArewicz@gmail.com",
                    IsDeleted = false,
                    UserName = "Antoni",
                    UserPassword = "unknown",
                },

                new()
                {
                    Id = 3,
                    CreatedOn = DateTime.Parse("2021-11-04T10:52:53.0870646+02:00"),
                    UserEmail = "roman.polko@gmail.com",
                    IsDeleted = false,
                    UserName = "Roman",
                    UserPassword = "unknown",
                },
            };
        }


        public Person GetPersonByUserId(int userId) => persons.FirstOrDefault(p => p.Id == userId);

        public IEnumerable<Person> GetAllPersons() => persons;

        public Person GetPersonByEmail(string userEmial) => persons.FirstOrDefault(p => p.UserEmail == userEmial);

        public async Task<Person> AddPerson(Person person)
        {
            if (person == null || person.UserEmail.IsNullOrWhiteSpace())
                return null;

            if (!persons.Any(p => p.Id == person.Id || p.UserEmail == person.UserEmail))
                persons.Add(person);
            
            return person;
        }

        public bool DeletePerson(string userEmail)
        {
            var personToDelete = FindPersonByEmial(userEmail);

            if (personToDelete == null) 
                return false;

            persons.Remove(personToDelete);

            return true;
        }

        public bool UpdatePerson(Person person)
        {
            if (person == null || person.UserEmail.IsNullOrWhiteSpace())
                return false;

            var personToUpdate = FindPersonByEmial(person.UserEmail);

            if (personToUpdate == null)
                return false;

            var ind = FindPersonIndexByEmial(personToUpdate);

            persons[ind] = person;

            return true;
        }

        private Person FindPersonByEmial(string userEmail) => persons.FirstOrDefault(p => p.UserEmail == userEmail);

        private int FindPersonIndexByEmial(Person person) => persons.IndexOf(person);
    }
}
