﻿using BankMillenium.Service;
using BankMillenium.Interface;
using BankMillenium.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BankMillenium.Extensions;
using Microsoft.Extensions.Logging;

namespace BankMillenium.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PersonDetailsController : ControllerBase
    {
        private readonly IPersonService personService;
        private readonly ILogger logger;


        public PersonDetailsController(IPersonService productService, ILoggerFactory loggerFactory)
        {
            personService = productService;
            logger = loggerFactory.CreateLogger<PersonDetailsController>();
        }


        [HttpPost("AddPerson")]
        public async Task<Object> AddPerson([FromBody] Person person)
        {
            logger.LogInformation($"AddPerson person = {person.XmlSerializeToString()}");

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                await personService.AddPerson(person);

                return true;
            }
            catch (Exception)
            {

                return false;
            }
        }

        [HttpDelete("DeletePerson")]
        public bool DeletePerson(string userEmail)
        {
            logger.LogInformation($"DeletePerson userEmail = {userEmail}");

            try
            {
                personService.DeletePerson(userEmail);

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        [HttpPut("UpdatePerson")]
        public bool UpdatePerson(Person person)
        {
            logger.LogInformation($"UpdatePerson @person = {person.XmlSerializeToString()}");

            if (!ModelState.IsValid)
            {
                return false;
            }

            try
            {
                personService.UpdatePerson(person);

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        [HttpGet("GetPersonByEmail")]
        public Object GetPersonByEmail(string userEmail)
        {
            logger.LogInformation($"GetPersonByEmail userEmail = {userEmail}");

            var data = personService.GetPersonByEmail(userEmail);

            return data;
        }

        [HttpGet("GetAllPersons")]
        public Object GetAllPersons()
        {
            logger.LogInformation("GetAllPersons");

            var data = personService.GetAllPersons();

            return data;
        }
    }
}