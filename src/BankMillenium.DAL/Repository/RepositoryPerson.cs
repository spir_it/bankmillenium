﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BankMillenium.Data;
using BankMillenium.Interface;
using BankMillenium.Models;

namespace BankMillenium.Repository
{
    public class RepositoryPerson : IRepository<Person>
    {
        private readonly ApplicationDbContext dbContext;

        public RepositoryPerson(ApplicationDbContext applicationDbContext)
        {
            dbContext = applicationDbContext;
        }

        public async Task<Person> Create(Person person)
        {
            var obj = await dbContext.Persons.AddAsync(person);

            await dbContext.SaveChangesAsync();

            return obj.Entity;
        }

        public void Delete(Person person)
        {
            dbContext.Remove(person);

            dbContext.SaveChanges();
        }

        public IEnumerable<Person> GetAll() => dbContext.Persons.Where(x => !x.IsDeleted).ToList();

        public Person GetById(int id) => dbContext.Persons.FirstOrDefault(x => !x.IsDeleted && x.Id == id);

        public void Update(Person person)
        {

            dbContext.Persons.Update(person);

            dbContext.SaveChanges();
        }
    }
}
