﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BankMillenium.Interface
{
    public interface IRepository<T>
    {
        public Task<T> Create(T person);

        public void Update(T person);

        public IEnumerable<T> GetAll();

        public T GetById(int id);

        public void Delete(T person);

    }
}