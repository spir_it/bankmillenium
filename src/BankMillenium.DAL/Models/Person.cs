﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace BankMillenium.Models
{
    [Table("Persons", Schema = "dbo")]
    public record Person
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; init; }

        [Required]
        [Display(Name = "UserName")]
        [StringLength(100)]
        public string UserName { get; init; }

        [Required]
        [Display(Name = "UserPassword")]
        [StringLength(30)]
        public string UserPassword { get; init; }

        [Required]
        [Display(Name = "UserEmail")]
        [EmailAddress]
        public string UserEmail { get; init; }

        [Required]
        [Display(Name = "CreatedOn")]
        [DataType(DataType.DateTime)]
        public DateTime CreatedOn { get; init; } = DateTime.Now;

        [Required]
        [Display(Name = "IsDeleted")]
        public bool IsDeleted { get; init; } = false;
    }
}
