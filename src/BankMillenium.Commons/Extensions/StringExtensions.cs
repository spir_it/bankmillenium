﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankMillenium.Extensions
{
    public static class StringExtensions
    {
        public static bool IsNullOrEmpty(this string s) => String.IsNullOrEmpty(s);

        public static bool IsNullOrWhiteSpace(this string s) => String.IsNullOrWhiteSpace(s);
    }
}
