﻿using System;
using System.Diagnostics;
using System.IO;
using System.Runtime.Serialization;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using Newtonsoft.Json;

namespace BankMillenium.Extensions
{
    public static class SerializeExtensions
    {
        public static string XmlSerializeToString(this object objectInstance)
        {
            var xmlSerializer = new XmlSerializer(objectInstance.GetType());
            
            var sb = new StringBuilder();

            using (TextWriter textWriter = new StringWriter(sb))
            {
                xmlSerializer.Serialize(textWriter, objectInstance);
            }

            return sb.ToString();
        }

        public static object XmlDeserializeFromString(this string objectData, Type type)
        {
            object obj;

            using (TextReader textReader = (TextReader) new StringReader(objectData))
            {
                obj = new XmlSerializer(type).Deserialize(textReader);
            }

            return obj;
        }

        public static string SerializeJson<T>(this T obj) => JsonConvert.SerializeObject(obj);

        public static T DeserializeJson<T>(this string json) => JsonConvert.DeserializeObject<T>(json);
    }
}
