﻿using System.Collections.Generic;
using System.Threading.Tasks;
using BankMillenium.Models;

namespace BankMillenium.Service
{
    public interface IPersonService
    {
        Person GetPersonByUserId(int userId);
        IEnumerable<Person> GetAllPersons();
        Person GetPersonByEmail(string userEmial);
        Task<Person> AddPerson(Person person);
        bool DeletePerson(string userEmail);
        bool UpdatePerson(Person person);
    }
}