﻿using BankMillenium.Interface;
using BankMillenium.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankMillenium.Service
{
    public class PersonService : IPersonService
    {
        private readonly IRepository<Person> personsRepo;

        public PersonService(IRepository<Person> perosn)
        {
            personsRepo = perosn;
        }

        public Person GetPersonByUserId(int userId) 
            => personsRepo
                .GetAll()
                .FirstOrDefault(x => x.Id == userId);

        public IEnumerable<Person> GetAllPersons() 
            => personsRepo
                .GetAll()
                .ToList();

        public Person GetPersonByEmail(string userEmial) 
            => personsRepo
                .GetAll()
                .FirstOrDefault(x => x.UserEmail == userEmial);

        public async Task<Person> AddPerson(Person person) 
            => await personsRepo.Create(person);

        public bool DeletePerson(string userEmail)
        {
            try
            {
                var dataList = personsRepo
                    .GetAll()
                    .Where(x => x.UserEmail == userEmail)
                    .ToList();

                foreach (var item in dataList)
                {
                    personsRepo.Delete(item);
                }

                return true;
            }
            catch (Exception)
            {
                return true;
            }
        }

        public bool UpdatePerson(Person person)
        {
            try
            {
                personsRepo.Update(person);

                return true;
            }
            catch (Exception ex)
            {
                return true;
            }
        }
    }
}